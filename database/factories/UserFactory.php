<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $birthDateRange = rand(18, 50);
        return [
            'country_id' => function () {
                return \App\Models\Country::factory(1)->create()->first()->id;
            },
            'name' => $this->faker->name(),
            'birth_date' => date("Y-m-d", strtotime(" - $birthDateRange Year")),

        ];
    }

    /**
     * Indicate that the model's email address should be unverified.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function unverified()
    {
        return $this->state(function (array $attributes) {
            return [
                'email_verified_at' => null,
            ];
        });
    }
}
