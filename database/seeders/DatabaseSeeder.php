<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $countries = \App\Models\Country::factory(5)->create();
        foreach ($countries as $country) {
            \App\Models\User::factory(rand(2, 10))->create(['country_id' => $country->id]);
        }
    }
}
