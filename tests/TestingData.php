<?php

namespace Tests;


trait TestingData
{

    public function users($params = [], $count = 1)
    {
        return  \App\Models\User::factory($count)->create($params);
    }

    public function countries($params = [], $count = 1)
    {
        return  \App\Models\Country::factory($count)->create($params);
    }
}
