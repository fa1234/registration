<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Task;
use App\Models\User;
use Tests\TestingData;
use App\Services\ErrorMessage;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    use RefreshDatabase, TestingData;
    /**
     * A basic feature test example.
     *
     * @return void
     */

    /** @test */
    public function check_registration_functionality()
    {
        //check registration endpoint
        $response = $this->json('get', '/register', []);

        $response->assertStatus(200);

        //create country
        $country = $this->countries([], 1)->first();

        //case 1 - check registration required fields
        $response = $this->json('POST', '/register', [
            // 'name' => 'name',
            // 'country_id' => $country->id,
            // 'birth_date' => '1992-10-12',
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);
        $this->assertTrue(isset($data['errors']['name']));
        $this->assertTrue(isset($data['errors']['country_id']));
        $this->assertTrue(isset($data['errors']['birth_date']));

        // case 2 - check name min length
        $response = $this->json('POST', '/register', [
            'name' => 'name',
            'country_id' => $country->id,
            'birth_date' => '1992-10-12',
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);
        $this->assertTrue(isset($data['errors']['name']));

        // case 3 - check name max length
        $response = $this->json('POST', '/register', [
            'name' => 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Unde, ea distinctio quia explicabo quam numquam tenetur vitae consequuntur eveniet id accusantium eos hic quod aspernatur cupiditate culpa porro consequatur eligendi quisquam quo quaerat vero laudantium maiores laboriosam. Totam sit nam voluptatibus ut harum aperiam maxime eius porro, dolorum labore dolor facilis?',
            'country_id' => $country->id,
            'birth_date' => '1992-10-12',
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);
        $this->assertTrue(isset($data['errors']['name']));

        // case 4 - check invalid country id
        $response = $this->json('POST', '/register', [
            'name' => 'name1',
            'country_id' => 111,
            'birth_date' => '1992-10-12',
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);
        $this->assertTrue(isset($data['errors']['country_id']));

        // case 4 - date birth date format
        $birthDate = date("Y/m/d", strtotime("- 18 Year"));
        $response = $this->json('POST', '/register', [
            'name' => 'name1',
            'country_id' => $country->id,
            'birth_date' => $birthDate,
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);
        $this->assertTrue(isset($data['errors']['birth_date']));


        // case 5 - birth date needs to be at leas 18 yeas before
        $birthDate = date("Y-m-d", strtotime("- 15 Year"));
        $response = $this->json('POST', '/register', [
            'name' => 'name1',
            'country_id' => $country->id,
            'birth_date' => $birthDate,
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);
        $this->assertTrue(isset($data['errors']['birth_date']));

        //it should create user's record
        $birthDate = date("Y-m-d", strtotime("- 18 Year"));
        $response = $this->json('POST', '/register', [
            'name' => 'name1',
            'country_id' => $country->id,
            'birth_date' => $birthDate,
        ]);

        $response->assertStatus(302);

        $this->assertDatabaseHas('users', [
            'name' => 'name1',
            'country_id' => $country->id,
            'birth_date' => $birthDate
        ]);
    }

    /** @test */
    public function check_users_endpoint_functionality()
    {
        //create users
        $this->users([], 5);
        //check registration endpoint
        $response = $this->json('get', '/users', []);
        $response->assertStatus(200);

        $content = $response->getOriginalContent();

        $content = $content->getData();

        $this->assertEquals(count($content['users']), 5);

        $newcomerInterval = config('meta.newcomer_interval') + 1;
        //make some record old
        $users = User::limit(3)->get();
        foreach ($users as $user) {
            $user->created_at = date("Y-m-d H:i:s", strtotime("-$newcomerInterval minute"));
            $user->save();
        }

        //check that old registered users ar not presented on the page
        $response = $this->json('get', '/users', []);
        $response->assertStatus(200);

        $content = $response->getOriginalContent();
        $content = $content->getData();

        $this->assertEquals(count($content['users']), 2);
    }
}
