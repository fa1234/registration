<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
    <link
      href="https://fonts.googleapis.com/css?family=Noto Sans"
      rel="stylesheet"
    />

    <link rel="stylesheet" href="{{ asset('front_styles/css/style.css') }}" />
  </head>
  <body>
    @if(session('error') || (session('success')))
    <div id="alertContainer" class="{{ session('error') ? 'error' : 'success' }}">
            <strong>{{ session('error') ? session('error') : session('success') }}
    </div>
    @endif

    @yield('content')

    <script>
        window.addEventListener('DOMContentLoaded', (event) => {
            var element =  document.getElementById('alertContainer');
            if (typeof(element) != 'undefined' && element != null)
            {
                setTimeout(function(){
                    element.style.display = "none";
                }, 3000);
            }
        });

    </script>
  </body>
</html>
