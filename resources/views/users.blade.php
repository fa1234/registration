@extends('layouts.app')

@section('content')

    <div class="table-container">
        <h1>User Data Table</h1>
        <div class="table-header">
            <div>Name</div>
            <div>Country</div>
            <div>Birth Date</div>
        </div>
        <table>
            <tbody>
                @foreach ($users as $user)
                <tr>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->country->name }}</td>
                    <td>{{ $user->birth_date }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>

        {{ $users->links('vendor.pagination.bootstrap-4') }}
    </div>

@endsection
