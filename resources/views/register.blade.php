@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="side left-side">
        <div class="content-box">
            <h1>Hello friend!</h1>
            <p>Enter your personal details and start journey with us</p>
        </div>
        <div class="white-arrow"></div>
        </div>
        <div class="side right-side">
        <h1>Add a user</h1>
        <p>Type in your info</p>

        <div class="form">
            <form method="POST" action="">
                @csrf
            <div class="form-group">
                <input type="text" name="name" id="" placeholder="Name" value="{{ old('name') }}" />
                @error('name')
                <div class="customValidate">
                    {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group form-select">
                <select name="country_id" class="form-select" id="">
                    <option value="">Select country</option>
                    @foreach ($countries as $country)
                        <option value="{{ $country->id }}" {{ old('country_id') == $country->id ? 'selected' : '' }}>{{ $country->name }}</option>
                    @endforeach
                </select>
                @error('country_id')
                    <div class="customValidate">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group form-date">
                <input type="date" name="birth_date" id="birth-date" placeholder="Date" value="{{ old('birth_date') }}" />
                @error('birth_date')
                    <div class="customValidate">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <input
                class="form-button"
                type="submit"
                name=""
                id=""
                value="Save"
                />
            </div>
            </form>
        </div>
        </div>
    </div>

@endsection
