<?php

namespace App\Http\Controllers;

use App\Models\Country;
use App\Http\Requests\RegisterRequest;
use App\Models\User;

class UserController extends Controller
{

    public function register()
    {
        $countries = Country::orderBy('name')->get();

        return view('register', compact('countries'));
    }

    public function store(RegisterRequest $request)
    {
        $user = new User();
        $user->name = $request->name;
        $user->country_id = $request->country_id;
        $user->birth_date = $request->birth_date;
        $user->save();

        return redirect()->route('users.index')->with('success', 'You registered Successfully');
    }

    public function users()
    {
        $users = User::newcomer()->with('country')->paginate(10);

        return view('users', compact('users'));
    }
}
